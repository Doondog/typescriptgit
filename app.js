console.log("It works!");
var myName = 'Donal';
//myName = 24;
var myAge = 27.5;
//myAge = 'Max'
var hasHobbies = true;
//hasHobbies = 1;
var myRealAge;
myRealAge = 27;
//myRealAge = '27';
var hobbies = ["Games", "Nerd Stuff"];
hobbies = [100];
//console.log(hobbies[0]);
// tuples
var address = ["SuperStreet", 99];
// enum
var Color;
(function (Color) {
    Color[Color["Gray"] = 0] = "Gray";
    Color[Color["Green"] = 100] = "Green";
    Color[Color["Blue"] = 101] = "Blue";
})(Color || (Color = {}));
var myColor = Color.Blue;
console.log(myColor);
// any
var car = "BMW";
console.log(car);
car = { brand: "BMW", series: 3 };
console.log(car);
//functions
function returnMyName() {
    return myName;
}
console.log(returnMyName());
function sayHello() {
    console.log("Hello!");
}
sayHello();
// arg types
function multiply(value1, value2) {
    return value1 * value2;
}
console.log(multiply(10, 2));
var myMultiply;
//myMultiply = sayHello;
//myMultiply();
myMultiply = multiply;
console.log(myMultiply(5, 2));
//let myMultiply: (val1: number, val2: number) => number;
//objects
var userData = {
    name: "Donal",
    age: 27
};
var complex = {
    data: [100, 3.99, 10],
    output: function (all) {
        return this.data;
    }
};
var complex2 = {
    data: [100, 3.99, 10],
    output: function (all) {
        return this.data;
    }
};
