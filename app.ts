console.log("It works!");

let myName = 'Donal';
//myName = 24;

let myAge = 27.5;
//myAge = 'Max'

let hasHobbies = true;
//hasHobbies = 1;

let myRealAge: number;

myRealAge = 27;
//myRealAge = '27';

let hobbies: any[] = ["Games", "Nerd Stuff"];
hobbies = [100];
//console.log(hobbies[0]);

// tuples

let address: [string,number] = ["SuperStreet", 99];

// enum
enum Color {
	Gray,
	Green = 100,
	Blue
}

let myColor: Color = Color.Blue;
console.log(myColor);

// any

let car: any = "BMW";
console.log(car);
car = { brand: "BMW", series : 3};
console.log(car);

//functions

function returnMyName(): string {
	return myName;

}

console.log(returnMyName());

function sayHello(): void {
console.log("Hello!");
}

sayHello();

// arg types

function multiply (value1,value2): number{
		return value1 * value2;

}
console.log(multiply(10,2));

let myMultiply: (val1: number, val2: number) => number;

//myMultiply = sayHello;
//myMultiply();


myMultiply = multiply;
console.log(myMultiply(5,2));

//let myMultiply: (val1: number, val2: number) => number;
//objects
let userData: {name: string, age: number}  = {
	name: "Donal",
	age: 27

};


let complex: {data: number[], output: (all: boolean) => number[]} = {

	data:[100, 3.99, 10],

	output: function (all :boolean): number[]{
	return this.data;

	}


};


type Complex = {data: number[], output: (all: boolean) => number[]};


let complex2 : Complex ={
	data:[100, 3.99, 10],

	output: function (all :boolean): number[]{
	return this.data;

	}
};

